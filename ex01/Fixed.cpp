/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 17:25:48 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/21 21:26:12 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"
#include <iostream>
#include <cmath>

using std::cout; using std::endl;

const int Fixed::FRACTIONAL_BITS = 8;

Fixed::Fixed()
{
	cout << "Default constructor called" << endl;
	number = 0;
}

Fixed::~Fixed()
{
	cout << "Destructor called" << endl;
}

Fixed::Fixed(const Fixed &fixed)
{
	cout << "Copy constructor called" << endl;
	this->number = fixed.number;
}

Fixed::Fixed(int n)
{
	cout << "Int constructor called" << endl;
	this->number = n << FRACTIONAL_BITS;
}

Fixed::Fixed(float f)
{
	cout << "Float constructor called" << endl;
	// To shift the storable decimals of f to the integer part before casting:
	// (int)(f * (2^FRACTIONAL_BITS))
	this->number = roundf(f * (2 << (FRACTIONAL_BITS - 1)));
}

Fixed&
Fixed::operator=(const Fixed &fixed)
{
	cout << "Assignation operator called" << endl;
	this->number = fixed.number;
	return (*this);
}

int
Fixed::getRawBits() const
{
	cout << "getRawBits member function called" << endl;
	return (number);
}

void
Fixed::setRawBits(int const raw)
{
	cout << "setRawBits member function called" << endl;
	this->number = raw;
}

float
Fixed::toFloat() const
{
	return (this->number * 1.0 / (2 << (FRACTIONAL_BITS - 1)));
}

int
Fixed::toInt() const
{
	return (this->number >> FRACTIONAL_BITS);
}

ostream&
operator<<(ostream& os, const Fixed &fixed)
{
	os << fixed.toFloat();
	return (os);
}
