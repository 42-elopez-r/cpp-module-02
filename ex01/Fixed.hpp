/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 17:13:27 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/21 20:45:34 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
#define FIXED_HPP

#include <ostream>

using std::ostream;

class Fixed
{
	private:
		int number;
		static const int FRACTIONAL_BITS;
	public:
		Fixed();
		~Fixed();
		Fixed(const Fixed &fixed);
		Fixed(int n);
		Fixed(float f);
		Fixed& operator=(const Fixed &fixed);
		int getRawBits() const;
		void setRawBits(int const raw);
		float toFloat() const;
		int toInt() const;
};

ostream& operator<<(ostream& os, const Fixed &fixed);

#endif
