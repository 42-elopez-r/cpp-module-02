/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 17:25:48 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/22 23:20:51 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"
#include <cmath>


const int Fixed::FRACTIONAL_BITS = 8;

Fixed::Fixed()
{
	number = 0;
}

Fixed::~Fixed()
{
}

Fixed::Fixed(const Fixed &fixed)
{
	this->number = fixed.number;
}

Fixed::Fixed(int n)
{
	this->number = n << FRACTIONAL_BITS;
}

Fixed::Fixed(float f)
{
	// To shift the storable decimals of f to the integer part before casting:
	// (int)(f * (2^FRACTIONAL_BITS))
	this->number = roundf(f * (2 << (FRACTIONAL_BITS - 1)));
}

Fixed&
Fixed::operator=(const Fixed &fixed)
{
	this->number = fixed.number;
	return (*this);
}

int
Fixed::getRawBits() const
{
	return (number);
}

void
Fixed::setRawBits(int const raw)
{
	this->number = raw;
}

float
Fixed::toFloat() const
{
	return (this->number * 1.0 / (2 << (FRACTIONAL_BITS - 1)));
}

int
Fixed::toInt() const
{
	return (this->number >> FRACTIONAL_BITS);
}

ostream&
operator<<(ostream& os, const Fixed &fixed)
{
	os << fixed.toFloat();
	return (os);
}

int
Fixed::operator>(const Fixed &fixed) const
{
	return (this->number > fixed.number);
}

int
Fixed::operator<(const Fixed &fixed) const
{
	return (this->number < fixed.number);
}

int
Fixed::operator>=(const Fixed &fixed) const
{
	return (this->number >= fixed.number);
}

int
Fixed::operator<=(const Fixed &fixed) const
{
	return (this->number <= fixed.number);
}

int
Fixed::operator==(const Fixed &fixed) const
{
	return (this->number == fixed.number);
}

int
Fixed::operator!=(const Fixed &fixed) const
{
	return (this->number != fixed.number);
}

Fixed
Fixed::operator+(const Fixed &fixed) const
{
	Fixed new_fixed;

	new_fixed.number = this->number + fixed.number;
	return (new_fixed);
}

Fixed
Fixed::operator-(const Fixed &fixed) const
{
	Fixed new_fixed;

	new_fixed.number = this->number - fixed.number;
	return (new_fixed);
}

Fixed
Fixed::operator*(const Fixed &fixed) const
{
	Fixed new_fixed;

	new_fixed.number = (this->number * fixed.number) >> FRACTIONAL_BITS;
	return (new_fixed);
}

Fixed
Fixed::operator/(const Fixed &fixed) const
{
	Fixed new_fixed(this->number * 1.0f / fixed.number);

	return (new_fixed);
}

Fixed&
Fixed::operator++()
{
	this->number++;
	return (*this);
}

Fixed
Fixed::operator++(int)
{
	Fixed temp(*this);

	this->number++;
	return (temp);
}

Fixed&
Fixed::operator--()
{
	this->number--;
	return (*this);
}

Fixed
Fixed::operator--(int)
{
	Fixed temp(*this);

	this->number--;
	return (temp);
}

Fixed&
Fixed::min(Fixed &f1, Fixed &f2)
{
	return (f1 < f2 ? f1 : f2);
}

const Fixed&
Fixed::min(const Fixed &f1, const Fixed &f2)
{
	return (f1 < f2 ? f1 : f2);
}

Fixed&
Fixed::max(Fixed &f1, Fixed &f2)
{
	return (f1 > f2 ? f1 : f2);
}

const Fixed&
Fixed::max(const Fixed &f1, const Fixed &f2)
{
	return (f1 > f2 ? f1 : f2);
}
