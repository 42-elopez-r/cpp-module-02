/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 17:13:27 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/22 18:24:17 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
#define FIXED_HPP

#include <ostream>

using std::ostream;

class Fixed
{
	private:
		int number;
		static const int FRACTIONAL_BITS;
	public:
		Fixed();
		~Fixed();
		Fixed(const Fixed &fixed);
		Fixed(int n);
		Fixed(float f);
		Fixed& operator=(const Fixed &fixed);
		int getRawBits() const;
		void setRawBits(int const raw);
		float toFloat() const;
		int toInt() const;
		int operator>(const Fixed &fixed) const;
		int operator<(const Fixed &fixed) const;
		int operator>=(const Fixed &fixed) const;
		int operator<=(const Fixed &fixed) const;
		int operator==(const Fixed &fixed) const;
		int operator!=(const Fixed &fixed) const;
		Fixed operator+(const Fixed &fixed) const;
		Fixed operator-(const Fixed &fixed) const;
		Fixed operator*(const Fixed &fixed) const;
		Fixed operator/(const Fixed &fixed) const;
		Fixed& operator++();
		Fixed operator++(int);
		Fixed& operator--();
		Fixed operator--(int);
		static Fixed& min(Fixed &f1, Fixed &f2);
		static const Fixed& min(const Fixed &f1, const Fixed &f2);
		static Fixed& max(Fixed &f1, Fixed &f2);
		static const Fixed& max(const Fixed &f1, const Fixed &f2);
};

ostream& operator<<(ostream& os, const Fixed &fixed);

#endif
