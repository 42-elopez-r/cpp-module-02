/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 17:25:48 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/21 20:48:48 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"
#include <iostream>

using std::cout; using std::endl;

const int Fixed::FRACTIONAL_BITS = 8;

Fixed::Fixed()
{
	cout << "Default constructor called" << endl;
	number = 0;
}

Fixed::~Fixed()
{
	cout << "Destructor called" << endl;
}

Fixed::Fixed(const Fixed &fixed)
{
	cout << "Copy constructor called" << endl;
	this->number = fixed.number;
}

Fixed&
Fixed::operator=(const Fixed &fixed)
{
	cout << "Assignation operator called" << endl;
	this->number = fixed.number;
	return (*this);
}

int
Fixed::getRawBits() const
{
	cout << "getRawBits member function called" << endl;
	return (number);
}

void
Fixed::setRawBits(int const raw)
{
	cout << "setRawBits member function called" << endl;
	this->number = raw;
}
